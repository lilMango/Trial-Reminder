export class Trial {
    public Id : string;
    public Name : string;
    public ExpirationDate : string;

    constructor(id: string, name : string, expirationDate : string) {
        this.Id = id;
        this.Name = name;
        this.ExpirationDate = expirationDate;
    }
}