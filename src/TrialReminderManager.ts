import {CustomWindow} from './CustomWindow';
import {Trial} from './models/Trial';
import {Guid} from 'guid-typescript';
import { resolve } from 'dns';

declare let window : CustomWindow;

window.browser = (function() {
    console.log("Determining browser type");
    return window.msBrowser ||
        window.browser ||
        window.chrome
})();

export class TrialReminderManager {
    public AddReminder(trial : Trial) {
        var key = trial.Id;
        window.browser.storage.sync.set({[key] : trial}, () => {
            console.log(`Added new trial ${trial} with id ${trial.Id}.`);
        })
    }
    public async RemoveReminder(id: string) : Promise<any> {
        return new Promise((resolve, reject) => {
            window.browser.storage.sync.remove(id, () => {
                if (window.browser.runtime.lastError) {
                    throw new Error(window.browser.runtime.lastError.message);
                }
                console.log(`Removed reminder ${id}.`);
            })
            resolve();
        });
    }

    public async GetReminders() : Promise<Array<Trial>> {
        return new Promise<Array<Trial>>((resolve, reject) => {
            console.log("Getting reminders from browser storage.");

            let reminders = new Array<Trial>();
            window.browser.storage.sync.get(null, (items) => {
                Object.keys(items).forEach(
                    reminder => reminders.push(items[reminder]));
                    resolve(reminders)
            });
        });
    }
}