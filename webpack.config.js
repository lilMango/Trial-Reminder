const path = require('path');
module.exports = {
    mode: 'development',
    entry: {
      index: ['./src/views/IndexView.ts', './src/views/AddReminderView.ts', './src/views/ViewTrialsView.ts']
    },
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          loader: 'ts-loader',
          exclude: /node_modules/
        },
        {
          test: /\.(sa|sc|c)ss$/,
          use: ['style-loader', 'css-loader', 'sass-loader']
        }
      ]
    },
    resolve: {
      extensions: [ '.tsx', '.ts', '.js']
    },
    output: {
      filename: '[name].js',
      path: path.resolve(__dirname, 'dist/js')
    }
  };