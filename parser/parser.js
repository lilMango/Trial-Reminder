var membershipStateParser = {
	AMAZON : {
		MEMBER_RATE_REGEX :  /\$(([0-9]+\.[0-9][0-9])\/([a-z]+))/,
		MEMBER_BILL_DATE_REGEX : /(([JFMAJSOND][a-z]+) ([0-9]+), (20([0-9]+)))/,
		parseRate : function(string) {

		},
		parseBillDate : function(string) {
			
		},
		/**
		* Parses for membership state.
		* Args: the text after passing the HTML node innerText. 
		*	ie. document.getElementById('primeCentralMembershipInfoMembershipType').innerText;
		* Returns "Prime","Trial", "Free"
		*/
		parseMembershipStatus: function (primeMemberStatusText) {			
			let isPrime = false;
			if (primeMemberStatusText === "Prime") {
				isPrime = true;
			}

			if (isPrime ===true) {
				return "Prime"
			} 
			//else if Trial
			//else Free
			return "Free";
		},
		/***
		 * Parses for further membership info
		 * Works for Prime members and Trial
		 * Returns: {membershipStatus, billDateInfo}
		 ***/
		parseMembershipInfo : function(memberStatus, memberInfoText) {
			let result = {};
			result['memberStatus'] = memberStatus;
			
			if(memberStatus === "Prime"){
				//let primeMemberInfoText = document.getElementById('payment-popup-action').innerText;
				let reg = memberInfoText.match(this.MEMBER_RATE_REGEX);
				result['bill_amt'] = reg[2];
				result['bill_freq'] = reg[3]				

				if (reg[3] === "year"){
					reg = memberInfoText.match(this.MEMBER_BILL_DATE_REGEX);				
					result['bill_date'] = {
						month: reg[2],
						day: reg[3],
						year: reg[4]
					}
				}

			}else if(memberStatus === "Trial"){

			}

			return result;
		},
		getMemberStatus : function(){
			let probablePrimeText = document.getElementById('primeCentralMembershipInfoMembershipType').innerText;
			let primeStatus = parseMembershipStatus(probablePrimeText);
			
			let primeInfoText = document.getElementById('payment-popup-action').innerText;

			return parseMembershipInfo(primeStatus,primeInfoText);
		}
	}
}

module.exports = membershipStateParser;