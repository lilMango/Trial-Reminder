var assert = require("chai").assert;
const AMAZON_PRIME_STRING = "Annual ($119.00/year)*\n* Price as of April 10, 2019\nBEST VALUE";
const PRIME_STATUS = "Prime"; //document.getElementById('primeCentralMembershipInfoMembershipType').innerText;

const {AMAZON} = require('../parser');

//https://www.regexpal.com/

describe("Amazon Member Parsing", function() {

    before(function() {

    });

    after(function() {

    });

    it("should be a Prime member", function() {
    	let res = AMAZON.parseMembershipStatus(PRIME_STATUS);
    	assert.equal("Prime",res);
    });

    it("is neither a Trial nor Prime member", function() {
    	let res = AMAZON.parseMembershipStatus("You are not prime.HEYOOO");
    	assert.equal("Free",res);
    });

    it("Prime Member rate, bill frequency", function() {          

    	let res = AMAZON.parseMembershipInfo(PRIME_STATUS,AMAZON_PRIME_STRING);
    	assert.equal('119.00',res['bill_amt']);
    	assert.equal('year',res['bill_freq'])

    });

    it("Parsed date - year", function() {

       	let res = AMAZON.parseMembershipInfo(PRIME_STATUS,AMAZON_PRIME_STRING);
		
		assert.equal('April',res['bill_date']['month']);
		assert.equal('10',res['bill_date']['day']);       	
		assert.equal('2019',res['bill_date']['year']);

    });
});