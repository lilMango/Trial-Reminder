/**
 * Usage:
 *
 *     # Configure tests to only run against Google Chrome.
 *     SELENIUM_BROWSER=chrome mocha -t 10000 amazon_test.js
 */

const assert = require('assert');
const {Browser, By, Key, until} = require('selenium-webdriver');
const {ignore, suite} = require('selenium-webdriver/testing');

const {AMAZON}= require('./credentials');

    


suite(function(env) {
  console.log('Please run: ');
  console.log('SELENIUM_BROWSER=chrome mocha -t 10000 amazon_test.js');
	let driver;
	
	let login = async function(user,pass) {
	      driver = await env.builder().build();	
	    	
	      await driver.get(AMAZON.member_url);
	      let flag = await driver.findElement(By.xpath("//input[@id='ap_email']")).isDisplayed();

	      if(flag===false) {
	      	throw new Error('no email input field');
	      }
	      await driver.findElement(By.xpath("//input[@id='ap_email']")).sendKeys(user, Key.RETURN); //enter username and enter (continue btn)

	      flag = await driver.findElement(By.xpath("//input[@id='ap_password']")).isDisplayed();
	      if(flag===false){
	      	throw new Error('no password field');
	      }

	      await driver.findElement(By.xpath("//input[@id='ap_password']")).sendKeys(pass, Key.RETURN);	
	}	

  describe('Amazon Test Suite', function() {
    
    it('Free member parsed?', async function() {

    	await login(AMAZON.free.user,AMAZON.free.pass);
      	//TODO call parser here
      	await driver.findElement(By.xpath("//div[@class='autoRenewWarningMessage']")).getText().then(function(text){
      		console.log(text);
      		assert.equal("You are not an Amazon Prime member.",text);      		      		
      	});

      	await driver.quit();
      	//await driver.wait(until.titleIs('webdriver - Google Search'), 1000);
    });

    it('Prime membership', async function() {

		await login(AMAZON.member.user,AMAZON.member.pass);

      //TODO call parser here
      	await driver.findElement(By.xpath("//span[@id='primeCentralMembershipInfoMembershipType']")).getText().then(function(text){
      		console.log(text);
      		assert.equal("Prime",text);      		      		
      	});
      	
      	await driver.findElement(By.xpath("//span[@id='payment-popup-action']")).getText().then(function(text){
      		console.log("payment message:"+text);
          assert.equal(text.indexOf("Annual ($119.00/year)** Price as of April 10, 2019"), -1);
      	});
      	
      	//TODO parse the date, and amount
      	await driver.quit()
      
    });

    it('TRIAL prime membership parsed?', async function() {

    	//assert(false);
      	
      	//await driver.quit()
      
    });        
  });
});



 
