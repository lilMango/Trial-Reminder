https://www.amazon.com/gp/primecentral?ie=UTF8&ref_=nav_youraccount_prime


Login:
document.getElementById('ap_email').value = <EMAIL NAME>  --$x("//input[@id='ap_email']");
document.getElementById('continue').childNodes[0].childNodes[0].click()  --$x("//input[@id='continue']");
document.getElementById('ap_password').value = <PASSWORD>
document.getElementById('signInSubmit').click()


For non-members:
document.getElementsByClassName('autoRenewWarningMessage')[0].textContent; 
--$x("//div[@class='autoRenewWarningMessage']")[0].innerText;

members:
$x("//span[@id='primeCentralMembershipInfoMembershipType']"); ==> 'Prime'
document.getElementsByClassName('primeCentralNextPaymentMessage')[0].innerText


USE REGEX:
Annual ($119.00/year)*
* Price as of April 10, 2019
BEST VALUE

(for the price)
const reg = /\$(([0-9]+(\.[0-9][0-9])?)\/[a-z]+)/; 

(for the date)
const reg = /(([JFMAJSOND][a-z]+) ([0-9]+), 20([0-9]+))/;
--------------------------------

Run:
```
SELENIUM_BROWSER=chrome mocha -t 10000 amazon_test.js
```
