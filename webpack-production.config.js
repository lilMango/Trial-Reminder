var WebpackStripLoader = require('strip-loader');
var devConfig = require('./webpack.config');

var stripLoader = {
    test: [/\.js$/, /\.ts$/],
    exclude: /node_modules/,
    loader: WebpackStripLoader.loader('console.log')
}

devConfig.module.rules.push(stripLoader);
devConfig.mode = 'production';

module.exports = devConfig;
